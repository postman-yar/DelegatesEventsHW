﻿using System.Collections;

namespace DelegatesEventsHW
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var myList = new List<MyClass>
            {
                new MyClass { A = 10, B = 2 },
                new MyClass { A = 1, B = 3 },
                new MyClass { A = 3, B = 9 },
                new MyClass { A = 1, B = 4 }
            };

            Func<MyClass, float> myFunc = a => a.A + a.B;

            Console.WriteLine($"Максимальный элемент в коллекции:  {myList.GetMax(myFunc)}");

            Console.WriteLine("-----------------------");

            var startDir = @"C:\Windows";
            const int MAX_DEPTH = 5;
            const int MAX_FILE_COUNT = 100;

            Console.WriteLine("Scan dir: " + startDir);
            var searcher = new FileSearcher(MAX_DEPTH);
            searcher.FileFoundEvent += FileReceiver;
            searcher.Search(startDir, "", 0);

            Console.WriteLine("Все файлы найдены.");

            void FileReceiver(object sender, FileFoundArgs file)
            {
                FileSearcher _searcher = (FileSearcher)sender;
                if (_searcher.FileCount >= MAX_FILE_COUNT)
                    _searcher.CancelRequested = true;
                Console.WriteLine(file.FoundFile);
            }
        }

        public static T? GetMax<T>(this IEnumerable e, Func<T, float> getParameter) where T : class
        {
            var listOfE = new List<T>();

            foreach (var item in e)
                listOfE.Add((T)item);

            if (listOfE.Count == 0)
                return null;

            var maxWeight = listOfE.Max(o => getParameter(o));
            var maxE = listOfE.Where(o => getParameter(o) == maxWeight).FirstOrDefault();
            return maxE;
        }
    }
}
