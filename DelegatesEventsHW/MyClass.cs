﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesEventsHW
{
    public class MyClass
    {
        public int A { get; set; }
        public int B { get; set; }
        public override string ToString()
        {
            return $"A = {A}, B = {B}";
        }
    }
}
