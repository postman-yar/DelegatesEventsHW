﻿namespace DelegatesEventsHW
{
    public class FileSearcher
    {
        public event EventHandler<FileFoundArgs>? FileFoundEvent;
        public bool CancelRequested { get; set; }
        private readonly int _maxDepth;
        public int FileCount { get; set; }

        public FileSearcher(int maxDepth)
        {
            _maxDepth = maxDepth;
        }

        public void Search(string directory, string searchPattern, int depth)
        {
            depth++;

            if (depth >= _maxDepth || CancelRequested)
                return;

            var subDir = Directory.GetDirectories(directory);

            foreach (var dir in subDir)
            {
                try
                {
                    Search(dir, searchPattern, depth);
                }
                catch (Exception e)
                {
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine($"{dir} - {e.Message}");
                    Console.ResetColor();
                }
            }

            foreach (var file in Directory.EnumerateFiles(directory, searchPattern))
            {
                FileCount++;

                var args = new FileFoundArgs(file);

                FileFoundEvent?.Invoke(this, args);
            }
        }
    }
}
